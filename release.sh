#!/usr/bin/env bash
# unofficial bash strict mode
# https://archive.fo/pINgs#selection-169.0-201.0
set -euo pipefail
IFS=$'\n\t'

rm -rf dist build
python setup.py sdist bdist_wheel
twine check dist/*
twine upload dist/*
